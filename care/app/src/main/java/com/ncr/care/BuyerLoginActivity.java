package com.ncr.care;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class BuyerLoginActivity extends AppCompatActivity {
    private EditText mEmail, mPassword;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener firebaseAithListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_login);

        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);

        Button mLogin = (Button) findViewById(R.id.login);
        Button mRegistration = (Button) findViewById(R.id.registration);
        Button mFaceLogin = (Button)findViewById(R.id.face_login);

        mAuth = FirebaseAuth.getInstance();

        /*mRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("BuyerLoginActivity", "Inside listener");
                Intent intent = new Intent(BuyerLoginActivity.this, BuyerRegistrationActivity.class);
                startActivity(intent);
                finish();
            }
            @Override
            public void onClick(View v) {
                final String email = mEmail.getText().toString();
                final String password = mPassword.getText().toString();


                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(BuyerLoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful()){
                            Toast.makeText(BuyerLoginActivity.this, "Sign Up Error", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            String user_id = mAuth.getCurrentUser().getUid();
                            DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Users").child("Buyers").child(user_id);
                            current_user_db.setValue(true);
                        }
                    }
                });

            }
        });*/

        firebaseAithListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user != null){
                    Intent intent = new Intent(BuyerLoginActivity.this, BuyerTabActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }
            }
        };

        mFaceLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuyerLoginActivity.this, CameraActivity.class);
                startActivity(intent);
                finish();
                }
                });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = mEmail.getText().toString();
                final String password = mPassword.getText().toString();

                if(email.matches("") ||password.matches("")){
                    Toast.makeText(BuyerLoginActivity.this, "Email or Password empty!", Toast.LENGTH_SHORT).show();
                    return;
                }

                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(BuyerLoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful()){
                            Toast.makeText(BuyerLoginActivity.this, "Sign In Error", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            //Intent intent = new Intent(BuyerLoginActivity.this, BuyerMapActivity.class);
                            Intent intent = new Intent(BuyerLoginActivity.this, BuyerTabActivity.class);
                            startActivity(intent);
                            finish();
                            return;
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(firebaseAithListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(firebaseAithListener);
    }
}

