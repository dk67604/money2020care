package com.ncr.care;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import Models.Offer;

public class AcceptOfferAdapter extends RecyclerView.Adapter<AcceptOfferAdapter.ViewHolder> {
    private Context context;
    private List<Offer> offerList;
    private OnItemClickListener mlistner;

    public interface OnItemClickListener{
        void OnBuyClick(int pos);
    }

    public void setOnItemClickListner(OnItemClickListener listner){
        mlistner = listner;
    }

    public AcceptOfferAdapter(Context context, List<Offer> list) {
        this.context = context;
        offerList = list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.accepted_offers_tile, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull AcceptOfferAdapter.ViewHolder holder, int position) {
        final Offer offer = offerList.get(position);
        holder.sellerName.setText(offer.getSellerName());
        holder.sellerRating.setRating(offer.getSellerRating());
        holder.material1.setText(offer.getMaterial1());
        holder.material2.setText(offer.getMaterial2());
        holder.weight1.setText(offer.getWeight1());
        holder.weight2.setText(offer.getWeight2());
        Picasso.get()
                .load("http://atlantaseo.marketing/wp-content/uploads/avatar-1.png")
                .into(holder.sellerImage);
    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView material1, material2, weight1, weight2, sellerName;
        ImageView sellerImage;
        RatingBar sellerRating;
        Button buy;
        public ViewHolder(View itemView) {
            super(itemView);

            sellerImage = (ImageView) itemView.findViewById(R.id.imgao);
            material1 = (TextView) itemView.findViewById(R.id.tvaoMaterial1);
            material2 = (TextView) itemView.findViewById(R.id.tvaoMaterial2);

            weight1 = (TextView) itemView.findViewById(R.id.tvaoprice1);
            weight2 = (TextView) itemView.findViewById(R.id.tvaoprice2);

            sellerName = (TextView) itemView.findViewById(R.id.tvaoName);
            sellerRating = (RatingBar) itemView.findViewById(R.id.rbao);
            //buy = (Button) itemView.findViewById(R.id.aoBuy);



        }


    }
}
