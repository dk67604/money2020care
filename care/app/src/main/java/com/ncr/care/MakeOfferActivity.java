package com.ncr.care;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class MakeOfferActivity extends AppCompatActivity {

    ImageView buyerImage;
    RatingBar buyerRating;
    TextView buyerName, material1, material2, price1, price2,totalPrice;
    EditText weight1, weight2;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_offer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        buyerRating = (RatingBar) this.findViewById(R.id.rbMO);
        buyerImage = (ImageView) this.findViewById(R.id.imgMO);
        buyerName = (TextView) this.findViewById(R.id.tvMOName);
        material1 = (TextView) this.findViewById(R.id.tvMOmat1);
        material2 = (TextView) this.findViewById(R.id.tvMOmat2);
        weight1 = (EditText) this.findViewById(R.id.etMOw1);
        weight2 = (EditText) this.findViewById(R.id.etMOw2);
        price1 = (TextView) this.findViewById(R.id.tvMOp1);
        price2 = (TextView) this.findViewById(R.id.tvMOp2);
    totalPrice = (TextView) this.findViewById(R.id.tvMOtp);
        final Intent intent = getIntent();
        if (intent != null) {
            buyerName.setText(intent.getStringExtra("buyerName"));
            material1.setText(intent.getStringExtra("material1"));
            material2.setText(intent.getStringExtra("material2"));
            buyerRating.setRating(intent.getIntExtra("buyerRating",0));
            Picasso.get()
                    .load("http://atlantaseo.marketing/wp-content/uploads/avatar-1.png")
                    .into(buyerImage);
       }
       final int  p1 = Integer.valueOf(intent.getStringExtra("price1"));
       final int p2   =      Integer.valueOf(intent.getStringExtra("price2"));
        float total = 0;


       weight1.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {

                Double w1 = Double.parseDouble(weight1.getText().toString());
                price1.setText("$ "+w1*p1);


           }

           @Override
           public void afterTextChanged(Editable s) {

           }
       });
        weight2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Double w2 = Double.parseDouble(weight2.getText().toString());
                price2.setText("$ "+w2*p2);
                double w1change = Double.parseDouble(weight1.getText().toString());
                double p1change = w1change*p1;

                totalPrice.setText("$ "+(p1change+(w2*p2)));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        }

    }

