package com.ncr.care;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class LockPriceAdapter extends BaseAdapter {

    private Activity mContext;
    private List<String> materials;
    private List<String> prices;
    private LayoutInflater mLayoutInflater = null;

    public LockPriceAdapter(Activity context, List<String> materials, List<String> prices) {
        mContext = context;
        this.materials = materials;
        this.prices = prices;
        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return materials.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v=view;
        CompleteListViewHolder viewHolder;
        if (view == null) {
            LayoutInflater li = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = li.inflate(R.layout.materialpricelayout, null);
            viewHolder = new CompleteListViewHolder(v);
            v.setTag(viewHolder);
        }else {
            viewHolder = (CompleteListViewHolder) v.getTag();
        }
        viewHolder.materialName.setText(materials.get(i));
        viewHolder.price.setText("$ "+prices.get(i));
        return v;
    }

    class CompleteListViewHolder {
        public TextView materialName;
        public TextView price;

        public CompleteListViewHolder(View base) {
            materialName = (TextView) base.findViewById(R.id.tvMaterial);
            price = (TextView) base.findViewById(R.id.tvPrice);
        }



    }
}