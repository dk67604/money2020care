package com.ncr.care;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import Models.Offer;

public class BuyerOfferAdapter extends RecyclerView.Adapter<BuyerOfferAdapter.ViewHolder> {
    private Context context;
    private List<Offer> offerList;
    private OnItemClickListener mlistner;

    public interface OnItemClickListener{
        void OnAcceptOfferClick(int pos);
    }
    public void setOnItemClickListner(OnItemClickListener listner){
        mlistner = listner;
    }
    public BuyerOfferAdapter(Context context, List<Offer> list) {
        this.context = context;
        offerList = list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.buyers_offers_tile, parent, false);
        return new ViewHolder(v, mlistner);
    }

    @Override
    public void onBindViewHolder(@NonNull BuyerOfferAdapter.ViewHolder holder, final int position) {
    final Offer offer = offerList.get(position);
        holder.sellerName.setText(offer.getSellerName());
        holder.sellerRating.setRating(offer.getSellerRating());
        holder.material1.setText(offer.getMaterial1());
        holder.material2.setText(offer.getMaterial2());
         holder.weight1.setText(offer.getWeight1());
        holder.weight2.setText(offer.getWeight2());
        Picasso.get()
                .load("http://atlantaseo.marketing/wp-content/uploads/avatar-1.png")
                .into(holder.sellerImage);

        holder.acceptOfferButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offerList.remove(position);




            }
        });

    }

    @Override
    public int getItemCount() {
        return offerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView material1, material2, weight1, weight2, sellerName;
        ImageView sellerImage;
        RatingBar sellerRating;
        Button acceptOfferButton;
        public ViewHolder(View itemView, final BuyerOfferAdapter.OnItemClickListener listner) {
            super(itemView);

            sellerImage = (ImageView) itemView.findViewById(R.id.imgBOT);
            material1 = (TextView) itemView.findViewById(R.id.tvBOTMaterial1);
            material2 = (TextView) itemView.findViewById(R.id.tvBOTMaterial2);

            weight1 = (TextView) itemView.findViewById(R.id.tvBOTweight1);
            weight2 = (TextView) itemView.findViewById(R.id.tvBOTweight2);

            sellerName = (TextView) itemView.findViewById(R.id.tvBOTName);
            sellerRating = (RatingBar) itemView.findViewById(R.id.rbBOT);
            acceptOfferButton = (Button) itemView.findViewById(R.id.bBOTAccept);



        }


    }
}
