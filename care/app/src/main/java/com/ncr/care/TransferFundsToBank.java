package com.ncr.care;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class TransferFundsToBank extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_funds_to_bank);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TransferFundsToBank.this, SellerTabActivity.class);
        startActivity(intent);
        finish();
    }
}
