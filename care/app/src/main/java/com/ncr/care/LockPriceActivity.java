package com.ncr.care;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class LockPriceActivity extends AppCompatActivity {

    private Button button;
    private Button lockButton;
    private ListView listView;
    private ArrayList<String> materials;
    private ArrayList<String> prices;
    private EditText etMaterial;
    private EditText etPrice;
    private LockPriceAdapter lockPriceAdapter;
    boolean isLock = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock_price);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_lock_price);
        setSupportActionBar(toolbar);
        initViews();
        materials = new ArrayList<>();
        prices = new ArrayList<>();
        lockPriceAdapter = new LockPriceAdapter(this, materials,prices);
        listView.setAdapter(lockPriceAdapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMaterialAndPrice(etMaterial.getText().toString(), etPrice.getText().toString());
                lockPriceAdapter.notifyDataSetChanged();
            }
        });

        lockButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(isLock){
                    v.setBackgroundResource(R.drawable.lock_foreground);
                }else{
                    v.setBackgroundResource(R.drawable.unlock_foreground);
                }

                isLock = !isLock; // reverse
            }
        });






    }
    private void initViews() {
        button=(Button) findViewById(R.id.bLPAddMaterial);
        listView=(ListView) findViewById(R.id.etLPListview);
        etMaterial= (EditText) findViewById(R.id.etLPMaterial);
        etPrice= (EditText) findViewById(R.id.etLPPrice);
        lockButton = (Button)findViewById(R.id.bLockPrice);
    }
    private void addMaterialAndPrice(String material, String price){
    materials.add(material);
        prices.add(price);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LockPriceActivity.this, BuyerTabActivity.class);
        startActivity(intent);
        finish();
    }


}
