package com.ncr.care;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Models.Offer;

public class AcceptedOfferActivity extends AppCompatActivity {
    private String url = "https://jsonplaceholder.typicode.com/todos/";
    //private String url = "http://ec2-34-203-228-40.compute-1.amazonaws.com/api";
    private RecyclerView mList;

    private LinearLayoutManager linearLayoutManager;
    private DividerItemDecoration dividerItemDecoration;
    private List<Offer> offerList;
    private AcceptOfferAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accepted_offer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mList = findViewById(R.id.rvao);
        offerList = new ArrayList<>();
        adapter = new AcceptOfferAdapter(getApplicationContext(),offerList);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());
        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);
        getData();


    }
    private void getData() {
        final ProgressDialog progressDialog = new ProgressDialog(getApplicationContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new com.android.volley.Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {
                Log.d("gg",response.toString());
                for (int i = 0; i < 5; i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        Offer offer = new Offer();
                        offer.setSellerName("Yash");
                        offer.setSellerRating(jsonObject.getInt("id"));
                        offer.setMaterial1(jsonObject.getString("title"));
                        offer.setWeight1("5");
                        offer.setMaterial2(jsonObject.getString("title"));
                        offer.setWeight2("50");


//
//                        JSONObject material1JsonObj = jsonObject.getJSONObject("material1");
//                        buyer.setMaterial1(material1JsonObj.getString("name"));
//                        buyer.setPrice1(material1JsonObj.getString("price"));
//
//                        JSONObject material2JsonObj = jsonObject.getJSONObject("material2");
//                        buyer.setMaterial2(material2JsonObj.getString("name"));
//                        buyer.setPrice2(material2JsonObj.getString("price"));
//
//                        JSONObject material3JsonObj = jsonObject.getJSONObject("material3");
//                        buyer.setMaterial3(material3JsonObj.getString("name"));
//                        buyer.setPrice3(material3JsonObj.getString("price"));
//
//                        JSONObject material4JsonObj = jsonObject.getJSONObject("material4");
//                        buyer.setMaterial4(material4JsonObj.getString("name"));
//                        buyer.setPrice4(material4JsonObj.getString("price"));

                        offerList.add(offer);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }
                adapter.notifyDataSetChanged();
                progressDialog.dismiss();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                progressDialog.dismiss();
            }
        });
//        JsonObjectRequest req = new JsonObjectRequest(url, null,
//                new com.android.volley.Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            Log.d("LOGG",response.getString("status"));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }, new com.android.volley.Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                VolleyLog.e("Error: ", error.getMessage());
//            }
//        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(jsonArrayRequest);
    }
}
