package com.ncr.care;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class SellerMapFragment extends Fragment {
    private SupportMapFragment mapFragment;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_seller_map, container, false);

        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.setMinZoomPreference(15.0f);
                    LatLng latLng1 = new LatLng(36.121427, -115.169519);
                    googleMap.addMarker(new MarkerOptions().position(latLng1)
                            .title("Venetian"));
                    LatLng latLng2 = new LatLng(36.121404, -115.17388);
                    googleMap.addMarker(new MarkerOptions().position(latLng2)
                            .title("OAK"));
                    LatLng latLng3 = new LatLng(36.119647, -115.169519);
                    googleMap.addMarker(new MarkerOptions().position(latLng3)
                            .title("Rhumbar"));
                    //googleMap.animateCamera(CameraUpdateFactory.zoomTo(17.0f));
                    googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng1));
                }
            });
        }

        // R.id.map is a FrameLayout, not a Fragment
        getChildFragmentManager().beginTransaction().replace(R.id.seller_map,  mapFragment).commit();
        return rootView;
    }
}
