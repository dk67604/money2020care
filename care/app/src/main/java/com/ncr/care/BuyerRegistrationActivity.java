package com.ncr.care;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.google.android.gms.common.internal.safeparcel.SafeParcelable.NULL;

public class BuyerRegistrationActivity extends AppCompatActivity {

    private EditText mEmail, mPassword, mPhoneNumber, mName;
    private Button mRegister;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener firebaseAithListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buyer_registration);

        mEmail = (EditText) findViewById(R.id.etEmail);
        mPassword = (EditText) findViewById(R.id.etPassword);
        mPhoneNumber = (EditText) findViewById(R.id.etPhonenumber);
        mName = (EditText) findViewById(R.id.etName);

        mRegister = (Button) findViewById(R.id.bSRegister);

        mAuth = FirebaseAuth.getInstance();

        /*firebaseAithListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if(user != null){
                    Intent intent = new Intent(BuyerRegistrationActivity.this, BuyerMapActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }
            }
        };*/

        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = mEmail.getText().toString();
                final String password = mPassword.getText().toString();
                final String number = mPhoneNumber.getText().toString(); //not yet going to db
                final String name = mName.getText().toString(); // not yet going to db

                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(BuyerRegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful()){
                            Toast.makeText(BuyerRegistrationActivity.this, "Sign Up Error", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            String user_id = mAuth.getCurrentUser().getUid();
                            DatabaseReference current_user_db = FirebaseDatabase.getInstance().getReference().child("Users").child("Buyers").child(user_id);
                            current_user_db.setValue(true);
                            Toast.makeText(BuyerRegistrationActivity.this, "Sign Up Successful", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(BuyerRegistrationActivity.this, BuyerLoginActivity.class);
                            startActivity(intent);
                            finish();
                            return;
                        }
                    }
                });

                //if(email == NULL || password == NULL ) //not working yet
                //  Toast.makeText(BuyerRegistrationActivity.this, "Email or Password Empty", Toast.LENGTH_SHORT).show();

                //else {


                //}

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(firebaseAithListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(firebaseAithListener);
    }
}
