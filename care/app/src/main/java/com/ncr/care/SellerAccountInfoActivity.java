package com.ncr.care;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SellerAccountInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_account_info);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SellerAccountInfoActivity.this, SellerTabActivity.class);
        startActivity(intent);
        finish();
    }
}
