package com.ncr.care;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import Models.Buyer;


public class SellersBuyerAdapter extends RecyclerView.Adapter<SellersBuyerAdapter.ViewHolder> {


    private Context context;
    private List<Buyer> list;
    private OnItemClickListener mlistner;

    public interface OnItemClickListener{
        void OnMakeOfferClick(int pos);
    }

    public void setOnItemClickListner(OnItemClickListener listner){
mlistner = listner;
    }
    public SellersBuyerAdapter(Context context, List<Buyer> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.sellers_buyer_tile, parent, false);
        return new ViewHolder(v, mlistner);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    final Buyer buyer = list.get(position);
    holder.buyerName.setText(buyer.getName());
    holder.buyerRating.setRating(buyer.getRating());
    holder.material1.setText(buyer.getMaterial1());
    holder.material2.setText(buyer.getMaterial2());
    holder.price1.setText(buyer.getPrice1());
    holder.price2.setText(buyer.getPrice2());

        Picasso.get()
                .load("http://atlantaseo.marketing/wp-content/uploads/avatar-1.png")
                .into(holder.buyerImage);



        holder.makeOfferButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                         Intent intent = new Intent(context,MakeOfferActivity.class);
                        intent.putExtra("material1",buyer.getMaterial1());
                        intent.putExtra("material2",buyer.getMaterial2());
                        intent.putExtra("price1",buyer.getPrice1());
                        intent.putExtra("price2",buyer.getPrice2());
                        intent.putExtra("buyerRating",buyer.getRating());
                        intent.putExtra("buyerName", buyer.getName());
                        context.startActivity(intent);


            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView material1, material2,  price1, price2,  buyerName;
        ImageView buyerImage;
        RatingBar buyerRating;
        Button makeOfferButton;
        public ViewHolder(View itemView, final OnItemClickListener listner) {
            super(itemView);

            buyerImage = (ImageView) itemView.findViewById(R.id.imgSBT);
            material1 = (TextView) itemView.findViewById(R.id.tvSBTMaterial1);
            material2 = (TextView) itemView.findViewById(R.id.tvSBTMaterial2);
            price1 = (TextView) itemView.findViewById(R.id.tvSBTprice1);
            price2 = (TextView) itemView.findViewById(R.id.tvSBTprice2);
            buyerName = (TextView) itemView.findViewById(R.id.tvSBTName);
            buyerRating = (RatingBar) itemView.findViewById(R.id.rbSBT);
            makeOfferButton = (Button) itemView.findViewById(R.id.bSBTMakeoffer);



        }


    }




}
