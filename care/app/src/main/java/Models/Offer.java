package Models;

public class Offer {
    String offerId;
    String sellerName;
    int sellerRating;
    String material1;
    String material2;
    String weight1;
    String weight2;

    public Offer(){}

    public Offer(String offerId, String sellerName, int sellerRating, String material1, String material2, String weight1, String weight2) {
        this.offerId = offerId;
        this.sellerName = sellerName;
        this.sellerRating = sellerRating;
        this.material1 = material1;
        this.material2 = material2;
        this.weight1 = weight1;
        this.weight2 = weight2;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public int getSellerRating() {
        return sellerRating;
    }

    public void setSellerRating(int sellerRating) {
        this.sellerRating = sellerRating;
    }

    public String getMaterial1() {
        return material1;
    }

    public void setMaterial1(String material1) {
        this.material1 = material1;
    }

    public String getMaterial2() {
        return material2;
    }

    public void setMaterial2(String material2) {
        this.material2 = material2;
    }

    public String getWeight1() {
        return weight1;
    }

    public void setWeight1(String weight1) {
        this.weight1 = weight1;
    }

    public String getWeight2() {
        return weight2;
    }

    public void setWeight2(String weight2) {
        this.weight2 = weight2;
    }
}
