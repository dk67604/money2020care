package Models;

public class Buyer {
    //private String image;
    private String name;
    private int rating;
    private String material1;
    private String material2;
    private String material3;
    private String material4;
    private String price1;
    private String price2;
    private String price3;
    private String price4;

    public Buyer(){

    }

    public Buyer(String name, int rating, String material1, String material2, String material3, String material4, String price1, String price2, String price3, String price4) {
        this.name = name;
        this.rating = rating;
        this.material1 = material1;
        this.material2 = material2;
        this.material3 = material3;
        this.material4 = material4;
        this.price1 = price1;
        this.price2 = price2;
        this.price3 = price3;
        this.price4 = price4;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getMaterial1() {
        return material1;
    }

    public void setMaterial1(String material1) {
        this.material1 = material1;
    }

    public String getMaterial2() {
        return material2;
    }

    public void setMaterial2(String material2) {
        this.material2 = material2;
    }

    public String getMaterial3() {
        return material3;
    }

    public void setMaterial3(String material3) {
        this.material3 = material3;
    }

    public String getMaterial4() {
        return material4;
    }

    public void setMaterial4(String material4) {
        this.material4 = material4;
    }

    public String getPrice1() {
        return price1;
    }

    public void setPrice1(String price1) {
        this.price1 = price1;
    }

    public String getPrice2() {
        return price2;
    }

    public void setPrice2(String price2) {
        this.price2 = price2;
    }

    public String getPrice3() {
        return price3;
    }

    public void setPrice3(String price3) {
        this.price3 = price3;
    }

    public String getPrice4() {
        return price4;
    }

    public void setPrice4(String price4) {
        this.price4 = price4;
    }
}
